use samuelcamps;
CREATE TABLE GebruikerHeeftAlbum (
    Albums_Id INT NOT NULL,
    Gebruikers_Id INT NOT NULL,
    DatumToevoeging DATE not null,
    CONSTRAINT fk_GebruikerHeeftAlbum_Albums FOREIGN KEY (Albums_Id)
        REFERENCES Albums (Id),
    CONSTRAINT fk_GebruikerHeeftAlbum_Gebruikers FOREIGN KEY (Gebruikers_Id)
        REFERENCES Gebruikers (Id)
);
