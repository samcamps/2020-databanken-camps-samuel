USE samuelcamps;
SET sql_safe_updates = 0;
UPDATE Boeken SET Familienaam = "Niet gekend";
ALTER TABLE Boeken CHANGE Familienaam Familienaam VARCHAR(200) CHAR SET UTF8MB4 NOT NULL;
SET sql_safe_updates = 1;