-- een voorbeeld met een gegroepeerde kolom
USE samuelcamps;
SELECT AVG(Leeftijd)
FROM Honden
GROUP BY Geslacht
HAVING Geslacht = 'mannelijk';