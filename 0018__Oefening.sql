SELECT 
    CONCAT(SUBSTRING(Naam, 1, 1),
            '+',
            SUBSTRING(Baasje, 1, 1)) as Koppel
FROM
    Huisdieren
ORDER BY Leeftijd;