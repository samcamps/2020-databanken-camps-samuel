use samuelcamps;
SET SQL_SAFE_UPDATES = 0;
ALTER table Nummers ADD Royalties TINYINT UNSIGNED;
UPDATE Nummers
SET Royalties = ROUND(Duurtijd/60)
WHERE Genre = 'Klassiek';
UPDATE Nummers
SET Royalties = ROUND(Duurtijd/20)
WHERE Genre = 'Rock' OR Genre = 'Metal';
UPDATE Nummers
SET Royalties = ROUND(Duurtijd/15)
WHERE Genre = 'Rap';
UPDATE Nummers
SET Royalties = ROUND(Duurtijd/10)
WHERE Artiest Like '%Zeppelin';
SET SQL_SAFE_UPDATES = 1;
