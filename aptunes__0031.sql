-- het script om Albums te voorzien van een
--  foreign key waarmee je naar de artiest verwijst is aptunes__0031.sql (schrijf je zelf)

USE samuelcamps;
Alter table Albums ADD Artiesten_Id INT,
ADD constraint fk_Albums_Artiesten foreign key (Artiesten_Id) references Artiesten(Id);

