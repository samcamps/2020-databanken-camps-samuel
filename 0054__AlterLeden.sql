use samuelcamps;
set sql_safe_updates = 0;
ALTER TABLE Leden ADD Taken_Id INT,
ADD CONSTRAINT fk_Leden_Taken FOREIGN KEY (Taken_Id) 
REFERENCES Taken(Id);
UPDATE Leden
SET Taken_Id = 1
WHERE Naam = 'Bavo';
UPDATE Leden
SET Taken_Id = 2
WHERE Naam = 'Yannick';
UPDATE Leden
SET Taken_Id = 3
WHERE Naam = 'Max';
ALTER TABLE Leden CHANGE Taken_Id Taken_Id INT NOT NULL;

