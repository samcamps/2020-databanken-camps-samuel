USE samuelcamps;
DROP TABLE IF EXISTS Boeken;
CREATE TABLE Boeken (
    Voornaam VARCHAR(100),
    Titel VARCHAR(100) NOT NULL
);
INSERT INTO Boeken (Voornaam,Titel) 
VALUES
("aurelius", "Filosofie"),
("Gerard", "Heiddegers vraag naar de techniek"),
("Diderik", "Logicaboek");
