USE samuelcamps;
ALTER TABLE Boeken
ADD COLUMN Personen_Id INT,               -- = persoon die bij dit boek hoort
ADD CONSTRAINT fk_Boeken_Personen
  FOREIGN KEY (Personen_Id)
  REFERENCES Personen(Id);