use samuelcamps;
CREATE TABLE GebruikerHeeftNummer (
    Nummers_Id INT NOT NULL,
    Gebruikers_Id INT NOT NULL,
    Favoriet Bool not null,
    CONSTRAINT fk_GebruikerHeeftNummer_Nummers FOREIGN KEY (Nummers_Id)
        REFERENCES Nummers (Id),
    CONSTRAINT fk_GebruikerHeeftNummer_Gebruikers FOREIGN KEY (Gebruikers_Id)
        REFERENCES Gebruikers (Id)
);
