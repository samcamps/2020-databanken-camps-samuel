use samuelcamps;
ALTER TABLE Boeken ADD COLUMN Auteurs_Id INT,
ADD CONSTRAINT fk_Boeken_Auteurs FOREIGN KEY (Auteurs_Id) REFERENCES Auteurs(Id);
