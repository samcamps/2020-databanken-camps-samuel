-- een voorbeeld met een geaggregeerde waarde
USE samuelcamps;
SELECT Geslacht
FROM Honden
GROUP BY Geslacht
HAVING AVG(Leeftijd) > 4;