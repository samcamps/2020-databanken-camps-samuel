USE samuelcamps;
SET SQL_SAFE_UPDATES = 0;
UPDATE Boeken
SET Categorie = 'Geschiedenis'
WHERE Familienaam = 'Braudel' or
      Familienaam = 'Bernard' or
      Familienaam = 'Bloch';
SET SQL_SAFE_UPDATES = 1;