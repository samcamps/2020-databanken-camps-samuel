use samuelcamps;
set sql_safe_updates = 0;
UPDATE (Nummers
    INNER JOIN Albums ON Albums.Titel = Nummers.Album)
        INNER JOIN
    Artiesten ON Nummers.Artiesten_Id = Artiesten.Id 
SET 
    Albums.Artiesten_Id = Nummers.Artiesten_Id;
set sql_safe_updates = 1;