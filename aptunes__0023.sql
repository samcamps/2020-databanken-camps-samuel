-- Het blijkt dat erg lange klassieke nummers niet erg winstgevend zijn voor onze dienst. 
-- Toon daarom alfabetisch alle artiesten die klassieke nummers hebben,
-- maar enkel als hun klassieke nummers ook gemiddeld langer dan 8 minuten duren.
-- Noem je script aptunes__0023.sql. Tip: je hebt hier een combinatie van alle clausules nodig.  << niet per se 

USE samuelcamps;
SELECT 
    Artiest
FROM
    Nummers
GROUP BY Artiest , Genre
HAVING AVG(Duurtijd) > 480
    AND Genre = 'Klassiek'
ORDER BY Artiest;
