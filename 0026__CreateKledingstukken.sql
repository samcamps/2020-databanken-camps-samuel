USE samuelcamps;
CREATE TABLE Kledingstukken (
    Nummer INT NOT NULL,
    Soort ENUM('polo', 'broek', 'trui'),
    Formaat ENUM('small', 'medium', 'large')
);