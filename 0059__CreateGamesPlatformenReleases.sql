USE samuelcamps;
CREATE TABLE Platformen (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Naam VARCHAR(50)CHAR SET UTF8MB4 NOT NULL
);


CREATE TABLE Games (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Titel VARCHAR(50)CHARSET UTF8MB4 NOT NULL
);

CREATE TABLE Releases (
    Games_Id INT NOT NULL ,
    Platformen_Id INT NOT NULL,
    CONSTRAINT fk_Releases_Games FOREIGN KEY (Games_Id)
        REFERENCES Games (Id),
    CONSTRAINT fk_Releases_Platformen FOREIGN KEY (Platformen_Id)
        REFERENCES Platformen (Id)
);
