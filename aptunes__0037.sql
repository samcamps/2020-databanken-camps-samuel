use samuelcamps;
alter table Nummers ADD column Id int auto_increment primary key;  -- foreign key kan enkel naar ne primary key verwijzen dus moet nog toegevoegd worden

create table NummerOpAlbum(
Albums_Id int not null,
Nummers_Id int not null,
Tracknummer tinyint unsigned not null,
constraint fk_NummerOpAlbum_Album foreign key (Albums_Id) references Albums(Id),
constraint fk_NummerOpAlbum_Nummers foreign key (Nummers_Id) references Nummers(Id)
);
