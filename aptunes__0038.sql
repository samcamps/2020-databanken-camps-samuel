-- In deze tabel sla je volgende informatie op met een eigen script aptunes__0038.sql. (Om dit te doen zoek je met de hand de Id van het nummer en 
-- van het album op in hun tabellen en INSERT je hun combinatie in de nieuwe tabel NummerOpAlbum. De werkwijze is dus dezelfde als in script 36.)

use samuelcamps;

insert into NummerOpAlbum (Albums_Id, Nummers_Id, Tracknummer)
values
(2,3,4),
(9,27,2)
;