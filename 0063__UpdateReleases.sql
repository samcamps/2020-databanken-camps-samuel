USE samuelcamps;
Update Releases
INNER JOIN Platformen ON Releases.Platformen_Id = Platformen.Id
INNER JOIN Games ON Releases.Games_Id = Games.Id
SET Releasedatum = '2019-02-22'
WHERE Games.Titel = 'Anthem';

Update Releases
INNER JOIN Platformen ON Releases.Platformen_Id = Platformen.Id
INNER JOIN Games ON Releases.Games_Id = Games.Id
SET Releasedatum = '2019-03-22'
WHERE Games.Titel = 'Sekiro: Shadows Die Twice';

Update Releases
INNER JOIN Platformen ON Releases.Platformen_Id = Platformen.Id
INNER JOIN Games ON Releases.Games_Id = Games.Id
SET Releasedatum = '2019-03-08'
WHERE Games.Titel = 'Devil May Cry 5';

Update Releases
INNER JOIN Platformen ON Releases.Platformen_Id = Platformen.Id
INNER JOIN Games ON Releases.Games_Id = Games.Id
SET Releasedatum = '2018-10-02'
WHERE Games.Titel = 'MEga Man 11';
